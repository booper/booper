/* application.rs
 *
 * Copyright 2022 Jan Willem
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::subclass::prelude::*;
use gtk::prelude::*;
use gtk::{gio, glib};

use crate::config::VERSION;
use crate::BooperWindow;
mod imp {
    use std::cell::RefCell;

    use super::*;

    #[derive(Debug)]
    pub struct BooperApplication {
        pub booper_window: RefCell<Option<BooperWindow>>,
    }

    impl Default for BooperApplication {
        fn default() -> Self {
            Self {
                booper_window: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BooperApplication {
        const NAME: &'static str = "BooperApplication";
        type Type = super::BooperApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for BooperApplication {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.instance();
            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);

            obj.set_accels_for_action("app.palette-open", &["<ctrl><shift>p"]);
        }
    }

    impl ApplicationImpl for BooperApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self) {
            let application = self.instance();
            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = BooperWindow::new(&*application);
                self.booper_window.replace(Some(window.clone()));
                window.upcast()
            };

            // Ask the window manager/compositor to present the window
            window.present();
        }
    }

    impl GtkApplicationImpl for BooperApplication {}
    impl AdwApplicationImpl for BooperApplication {}
}

glib::wrapper! {
    pub struct BooperApplication(ObjectSubclass<imp::BooperApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl BooperApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::new(&[("application-id", &application_id), ("flags", flags)])
    }

    fn setup_gactions(&self) {
        let quit_action = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| app.quit())
            .build();
        let about_action = gio::ActionEntry::builder("about")
            .activate(move |app: &Self, _, _| app.show_about())
            .build();

        let palette_open = gio::ActionEntry::builder("palette-open")
            .activate(move |app: &Self, _, _| {
                app.imp()
                    .booper_window
                    .borrow()
                    .as_ref()
                    .expect("")
                    .imp()
                    .popover
                    .popup();
            })
            .build();

        self.add_action_entries([quit_action, about_action, palette_open])
            .unwrap();
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let about = adw::AboutWindow::builder()
            .transient_for(&window)
            .application_name("booper")
            .application_icon("io.gitlab.boober.Booper")
            .developer_name("Jan Willem")
            .version(VERSION)
            .developers(vec!["Jan Willem".into()])
            .copyright("© 2022 Jan Willem")
            .build();

        about.present();
    }
}
