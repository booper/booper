/* window.rs
 *
 * Copyright 2022 Jan Willem
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use crate::command_palette::BooperCommandPalette;
use adw::subclass::prelude::*;
use gtk::gdk::Display;
use gtk::prelude::*;
use gtk::{gio, glib};
use glib::clone;

mod imp {
    use crate::script_row::BooperScriptRow;

    use super::*;

    #[derive(Debug, gtk::CompositeTemplate)]
    #[template(resource = "/io/gitlab/boober/Booper/gtk/window.ui")]
    pub struct BooperWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub source_view: TemplateChild<sourceview::View>,
        #[template_child]
        pub source_buffer: TemplateChild<sourceview::Buffer>,
        pub popover: BooperCommandPalette,
    }

    impl Default for BooperWindow {
        fn default() -> Self {
            Self {
                header_bar: TemplateChild::default(),
                source_view: TemplateChild::default(),
                source_buffer: TemplateChild::default(),
                popover: BooperCommandPalette::new(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BooperWindow {
        const NAME: &'static str = "BooperWindow";
        type Type = super::BooperWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BooperWindow {
        fn constructed(&self) {
            self.parent_constructed();
            self.instance().load_css();
            self.popover.set_parent(&self.header_bar.clone());


            self.popover.imp().option_list.connect_row_activated(clone!(@weak self as this => move |list: &gtk::ListBox, row: &gtk::ListBoxRow| {
                this.popover.popdown();
                let script_row = row.clone().downcast::<BooperScriptRow>().expect("Row should be a script");
                let script = script_row.imp().script.borrow();
                let exec = script.as_ref().expect("");


                let t =  this.source_buffer.property_value("text").get::<String>().expect("");
                let c: char = char::from_u32(0).expect("");
                let res = exec.execute(t.as_str(), None).expect("").text();
                let res = res.as_str().trim_end_matches(c);
                this.source_buffer.set_text(res);
            }));
        }
    }
    
    impl WidgetImpl for BooperWindow {}
    impl WindowImpl for BooperWindow {}
    impl ApplicationWindowImpl for BooperWindow {}
    impl AdwApplicationWindowImpl for BooperWindow {}
}

glib::wrapper! {
    pub struct BooperWindow(ObjectSubclass<imp::BooperWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl BooperWindow {
    pub fn new<P: glib::IsA<adw::Application>>(application: &P) -> Self {
        glib::Object::new(&[("application", application)])
    }

    fn load_css(&self) {
        // Load the CSS file and add it to the provider
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/io/gitlab/boober/Booper/gtk/style.css");
        // Add the provider to the default screen
        gtk::StyleContext::add_provider_for_display(
            &Display::default().expect("Could not connect to a display."),
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
    }
}
