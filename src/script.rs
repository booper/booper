// Copyright 2020 Zoey Sheffield
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Jan Willem Eriks - 2023

use crate::executor::{ExecutionStatus, Executor, ExecutorError};
use crossbeam::channel::{bounded, Receiver, Sender};
use eyre::{Context, eyre, Result};
use log::{info, debug, warn};
use serde::Deserialize;
use std::{fmt, fs, path::PathBuf, thread};
use std::cell::{RefCell};

pub struct Script {
    pub metadata: Metadata,
    pub path: Option<PathBuf>,
    source: String,
    channel: RefCell<Option<ExecutorChannel>>,
}
#[derive(Debug)]
enum ExecutorJob {
    Request((String, Option<String>)),
    Responce(Result<ExecutionStatus, ExecutorError>),
    Kill,
}

struct ExecutorChannel {
    sender: Sender<ExecutorJob>,
    receiver: Receiver<ExecutorJob>,
}

#[derive(Debug)]
pub enum ParseScriptError {
    NoMetadata,
    InvalidMetadata(json::error::Error),
    FailedToRead(std::io::Error),
}

impl fmt::Display for ParseScriptError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ParseScriptError::NoMetadata => write!(f, "no metadata"),
            ParseScriptError::InvalidMetadata(e) => write!(f, "invalid metadata: {}", e),
            ParseScriptError::FailedToRead(e) => write!(f, "failed to read script: {}", e),
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct Metadata {
    pub api: u32,
    pub name: String,
    pub description: String,
    pub author: Option<String>,
    pub icon: String,
    pub tags: Option<String>,
}


impl Script {
    pub fn from_file(path: PathBuf) -> Result<Self, ParseScriptError> {
        match fs::read_to_string(path.clone()) {
            Ok(source) => {
                let metadata = Script::get_metadata(&source).or_else(|e| Err(e))?;
                Ok(Script {
                    metadata,
                    source,
                    channel: RefCell::new(None),
                    path: Some(path),
                })
            },
            Err(e) => Err(ParseScriptError::FailedToRead(e)),
        }
    }

    pub fn from_source(source: String) -> Result<Self, ParseScriptError> {
        let metadata = Script::get_metadata(&source).or_else(|e| Err(e))?;
        Ok(Script {
            metadata,
            source,
            channel: RefCell::new(None),
            path: None,
        })
    }

    fn get_metadata(source: &String)-> Result<Metadata, ParseScriptError> {
        let start = source.find("/**").ok_or(ParseScriptError::NoMetadata)?;
        let end = source.find("**/").ok_or(ParseScriptError::NoMetadata)?;

        let mut metadata: Metadata = json::from_str(&source[start + 3..end])
            .map_err(ParseScriptError::InvalidMetadata)?;

        metadata.icon = metadata.icon.to_lowercase();
        return Ok(metadata);

    }

    fn init_executor_thread(&self) {
        assert!(self.channel.borrow().is_none());

        let (sender, receiver) = bounded(0);

        {
            let t_name = self.metadata.name.clone();
            let t_source = self.source.clone();
            let (t_sender, t_receiver) = (sender.clone(), receiver.clone());

            thread::spawn(move || {
                info!("thread spawned for {}", t_name);

                let mut executor = None;

                debug!("executor created");

                loop {
                    match t_receiver.recv().unwrap() // blocks until receive 
                    {
                        ExecutorJob::Request((full_text, selection)) => {
                            if executor.is_none() {
                                executor = match Executor::new(&t_source) {
                                    Ok(executor) => Some(executor),
                                    Err(err) => {
                                        warn!("failed to create executor");
                                        let executor_err = err.downcast::<ExecutorError>().unwrap(); // anything else is unrecoverable
                                        t_sender.send(ExecutorJob::Responce(Err(executor_err)))
                                            .wrap_err("Failed to send error responce")
                                            .unwrap();
                                        None
                                    }
                                }
                            }

                            if let Some(executor) = executor.as_mut() {
                                info!(
                                    "request received, full_text: {} bytes, selection: {} bytes",
                                    full_text.len(),
                                    selection.as_ref().map(|s| s.len()).unwrap_or(0),
                                );
                                let result = executor
                                    .execute(&full_text, selection.as_deref())
                                    .map_err(|err| err.downcast::<ExecutorError>().unwrap());
                                t_sender.send(ExecutorJob::Responce(result)).unwrap(); // blocks until send
                            }
                        }
                        ExecutorJob::Responce(_) => {
                            warn!("executor thread received a responce on channel");
                        }
                        ExecutorJob::Kill => {
                            info!("killing thread for {}", t_name);
                            return;
                        }
                    }
                }
            });
        };

        self.channel.replace(Some(ExecutorChannel { sender, receiver }));
    }

    // kills the thread associated with this script, it will be recreated when `execute` is called
    pub fn kill_thread(&self) {
        if let Some(channel) = self.channel.borrow().as_ref() {
            channel.sender.send(ExecutorJob::Kill).unwrap(); // blocks until send
        }

        self.channel.replace(None);
    }

    pub fn execute(&self, full_text: &str, selection: Option<&str>) -> Result<ExecutionStatus> {
        if self.channel.borrow().is_none() {
            self.init_executor_thread();
        }
        assert!(self.channel.borrow().is_some());

        let binding = self
            .channel
            .borrow();
        let channel = binding
            .as_ref()
            .ok_or_else(|| eyre!("Channel is none"))?;

        // send request
        channel
            .sender
            .send(ExecutorJob::Request((
                full_text.to_owned(),
                selection.map(|s| s.to_owned()),
            )))
            .wrap_err("Channel is disconnected")?;

        // receive result
        let result = channel
            .receiver
            .recv()
            .wrap_err("Receive channel is empty and disconnected")?;

        if let ExecutorJob::Responce(status) = result {
            return status.map_err(eyre::Report::from);
        }

        Err(eyre!(
            "Expected a responce on channel, but got a request: {:?}",
            result
        ))
    }
}