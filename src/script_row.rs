// Copyright (C) 2023 janwillem
//
// This file is part of booper.
//
// booper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// booper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with booper.  If not, see <http://www.gnu.org/licenses/>.

use adw::subclass::prelude::*;
use gtk::glib;
use gtk::prelude::*;
use std::borrow::Borrow;
use std::cell::{Cell, RefCell};

mod imp {

    use std::borrow::Borrow;

    use gtk::glib::{once_cell::sync::Lazy, ParamSpec, ParamSpecString, Value};

    use crate::script::Script;

    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/io/gitlab/boober/Booper/gtk/script-row.ui")]
    pub struct BooperScriptRow {
        #[template_child]
        pub script_name: TemplateChild<gtk::Label>,
        #[template_child]
        pub description: TemplateChild<gtk::Label>,
        #[template_child]
        pub icon: TemplateChild<gtk::Image>,
        path: Cell<String>,
        pub script: RefCell<Option<Script>>,
        pub search_string: Cell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BooperScriptRow {
        const NAME: &'static str = "BooperScriptRow";
        type Type = super::BooperScriptRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BooperScriptRow {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> =
                Lazy::new(|| vec![ParamSpecString::builder("path").build()]);
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "path" => {
                    let path: String = value
                        .get()
                        .expect("The value needs to be of type `String`.");
                    let script = Script::from_file(path.clone().into());

                    if let Some(old_script) = self.script.borrow().as_ref() {
                        old_script.kill_thread();
                    }
                    self.script.replace(None);

                    if let Ok(script) = script {
                        self.script_name.set_label(script.metadata.name.as_str());
                        self.description
                            .set_label(script.metadata.description.as_str());

                        let mut icon_name = script.metadata.icon.clone();
                        icon_name.push_str("-symbolic");
                        self.icon.set_icon_name(Some(icon_name.as_str()));

                        let mut search_string =
                            script.metadata.name.clone() + script.metadata.description.as_str();
                        if let Some(tags) = script.metadata.tags.clone() {
                            search_string = search_string + tags.as_str();
                        }
                        search_string = to_alphabetic_lowercase(&search_string);
                        self.search_string.replace(search_string);
                        self.script.replace(Some(script));
                    }

                    self.path.replace(path);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "path" => self.path.borrow().clone().take().to_value(),
                _ => unimplemented!(),
            }
        }
    }
    impl WidgetImpl for BooperScriptRow {}
    impl ListBoxRowImpl for BooperScriptRow {}
}

glib::wrapper! {
    pub struct BooperScriptRow(ObjectSubclass<imp::BooperScriptRow>)
        @extends gtk::Widget, gtk::ListBoxRow,
        @implements gtk::Accessible, gtk::Actionable, gtk::Buildable;
}

impl BooperScriptRow {
    pub fn new(path: String) -> Self {
        glib::Object::builder().property("path", path).build()
    }

    pub fn filter(&self, input: &String) -> bool {
        let input = to_alphabetic_lowercase(&input);
        let search_string = self.imp().search_string.borrow().take();
        self.imp().search_string.replace(search_string.clone());
        return alphabetic_contains(&search_string, &input);
    }
}

fn alphabetic_contains(str1: &String, str2: &String) -> bool {
    return to_alphabetic_lowercase(str1).contains(&to_alphabetic_lowercase(str2));
}

fn to_alphabetic_lowercase(str: &String) -> String {
    return str
        .to_lowercase()
        .chars()
        .filter(|x| x.is_alphabetic())
        .collect::<String>();
}
