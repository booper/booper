/** command-palette.rs
 *
 * Copyright 2022 Jan Willem
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
use adw::subclass::prelude::*;
use glib::clone;
use gtk::glib;
use gtk::prelude::*;
mod imp {

    use std::fs;

    use gtk::{gdk::Key, Inhibit};

    use crate::script_row::BooperScriptRow;

    use super::*;

    #[derive(Debug, gtk::CompositeTemplate)]
    #[template(resource = "/io/gitlab/boober/Booper/gtk/command-palette.ui")]
    pub struct BooperCommandPalette {
        #[template_child]
        pub entry_buffer: TemplateChild<gtk::EntryBuffer>,
        #[template_child]
        pub option_list: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub search_field: TemplateChild<gtk::Entry>,
    }

    impl Default for BooperCommandPalette {
        fn default() -> Self {
            Self {
                entry_buffer: TemplateChild::default(),
                option_list: TemplateChild::default(),
                search_field: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for BooperCommandPalette {
        const NAME: &'static str = "BooperCommandPalette";
        type Type = super::BooperCommandPalette;
        type ParentType = gtk::Popover;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for BooperCommandPalette {
        fn constructed(&self) {
            self.parent_constructed();

            let paths = fs::read_dir("scripts/").unwrap();

            for path in paths {
                let path = path.expect("").path();
                if let Some(file_type) = path.extension() {
                    if file_type.eq("js") {
                        self.instance()
                            .imp()
                            .option_list
                            .append(&BooperScriptRow::new(path.to_str().expect("").to_string()));
                    }
                }
            }
            self.instance().imp().entry_buffer.connect_text_notify(
                clone!(@weak self as this => move |buffer: &gtk::EntryBuffer|{
                    this.instance().imp().option_list.invalidate_filter();
                }),
            );

            let this = self.instance().clone();
            self.instance()
                .imp()
                .option_list
                .set_filter_func(move |row: &gtk::ListBoxRow| {
                    let row = row.clone().downcast::<BooperScriptRow>().expect("");
                    let search = this.imp().entry_buffer.text();
                    let visable = row.filter(&search);

                    if !visable && row.is_selected() {
                        this.imp().option_list.select_row(gtk::ListBoxRow::NONE);
                    }
                    return visable;
                });

            // Allow for instant typing when command-palette opens
            self.instance()
                .connect_show(clone!(@weak self as this => move |_|{
                    this.search_field.grab_focus();
                }));

            // When the search field has no focus but the user types valid keys force focus on search field.
            let controller = gtk::EventControllerKey::new();
            controller.set_name(Some("Focus Controller"));
            let controll_keys = [
                Key::KP_Up,
                Key::KP_Down,
                Key::Up,
                Key::Down,
                Key::KP_Tab,
                Key::Tab,
                Key::Return,
                Key::KP_Enter,
                Key::Escape,
            ];
            let this = self.instance().clone();
            controller.connect_key_pressed(
                move |_, keyval: Key, _keycode: u32, _state: gtk::gdk::ModifierType| {
                    if controll_keys.contains(&keyval) {
                        return Inhibit(false);
                    }
                    this.imp().search_field.grab_focus();
                    return Inhibit(true);
                },
            );

            self.instance().add_controller(&controller);
        }
    }
    impl WidgetImpl for BooperCommandPalette {}
    impl PopoverImpl for BooperCommandPalette {}
}

glib::wrapper! {
    pub struct BooperCommandPalette(ObjectSubclass<imp::BooperCommandPalette>)
        @extends gtk::Widget, gtk::Popover,
        @implements gtk::Accessible, gtk::Actionable;
}

impl BooperCommandPalette {
    pub fn new() -> Self {
        glib::Object::builder().build()
    }
}
