/**
     {
         "api":1,
         "name":"Natural Sort Lines",
         "description":"Sort lines with smart handling of numbers.",
         "author":"Sebastiaan Besselsen",
         "icon":"sort-numbers",
         "tags":"sort,natural,natsort,buggy"
     }
 **/

function main(input) {
    const locale = new Intl.Locale("en-US");

    let list = input.text.split(/\r?\n/).filter((line) => line.length > 0);

    const ne = str => str.replace(/\d+/g, n => n.padStart(8, "0"));
    // const nc = (a,b) => ne(a).localeCompare(ne(b)); //TODO: LOCALECOMPARE
    list =list.map(ne).sort()
    // list.sort(nc)
    input.text = list.join('\n');
}
