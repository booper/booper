/**
     {
         "api":1,
         "name":"Sort lines",
         "description":"Sort lines alphabetically.",
         "author":"Jan Willem Eriks",
         "icon":"sort-characters",
         "tags":"sort,alphabet"
     }
 **/

function main(input) {
    const sortLines = input.text.split(/\r?\n/).filter((line) => line.length > 0).sort().join('\n');
    input.text = sortLines;
}
